Feature: can a user login into our system?
    As a user
    I want to be able to register and login into our system
    So that, only authenticated user can access to our system

    Background: initialized system
        Given a initialized system
        And an existent user database
            | username | password  |
            | daniel   | admin!234 |

    Scenario: A user log tries to register in the system, using an not secure password
        Given a user with username "root"
        When the user input password "123"
        And the user try to register
        Then the system will throw an exception

    Scenario: A user tries to register using a secure passsword
        Given a user with username "root"
        When the user input password "12345678"
        And the user try to register
        Then the system will not throw an exception

    Scenario: A database connection fails when a user log in
        Given a user with username "daniel"
        When the user input password "admin!234"
        And the database is offline
        And the user try to login
        Then the system will throw an exception

    Scenario Outline: a user tries to login and receive a token, only if the user exists on the database
        And a user with username "<username>"
        When the user input password "<password>"
        And the user try to login
        Then session token should be "<exists>"

        Examples:
            | username | password  | action | exists    |
            | daniel   | xas0122ds | not    | empty     |
            | daniel   | admin!234 | do     | not empty |
