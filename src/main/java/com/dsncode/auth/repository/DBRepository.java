package com.dsncode.auth.repository;

import java.util.HashMap;
import java.util.Map;

import com.dsncode.auth.vo.User;

public class DBRepository {

    Map<String, User> db = new HashMap<>();
    DBDriver driver;

    public DBRepository(DBDriver driver) {
        this.driver = driver;
    }

    protected void setDBDriver(DBDriver driver) {
        this.driver = driver;
    }

    public void registerUser(User user) throws Exception {
        driver.insert(user);
        this.db.put(user.username, user);
    }

    public boolean isUserOnDB(String username, String password) throws Exception {
        driver.query("");
        if (this.db.containsKey(username) && this.db.get(username).password.equals(password)) {
            return true;
        }
        return false;
    }

}