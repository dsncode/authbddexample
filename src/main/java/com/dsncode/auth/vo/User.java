package com.dsncode.auth.vo;

import java.util.UUID;

/**
 * User Value object
 */
public class User {

    public final UUID uuid;
    public final String username;
    public final String password;

    public static User createInstance(UUID uuid, String username, String password) throws Exception {

        if (password == null || password.isEmpty()) {
            throw new Exception("null or empty password");
        }
        return new User(uuid, username, password);
    } 

    private User(UUID uuid, String username, String password) {
 
        this.username = username;
        this.password = password;
        this.uuid = uuid;
    }

}